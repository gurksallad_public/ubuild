# ubuild

Warning
This software is unfinished. Keep your expectations low.

An experimental tool for building c and c++ projects without exernal buildscript or tools except a compiler.


## How it works

You create a .c or .cpp file including ubuild.h
#define UBUILD_IMPLEMENTATION before including ubuild.h (stb style header)
configure your script within the main function and then build it with no build configuration.
It then should output an executable that is capable of compiling you project, 
from then on you use that file to build your project.


### Example project
```
#define UBUILD_IMPLEMENTATION
#include "ubuild.h"

int main()
{
    cmd command = {};

    ubSetBuildOutput(&command, "Build", "MyAppName");
    
    ubSetDefaultCompiler(&command);
    ubAddIncludePath(&command, "./usr/include/sdl");
    ubAddIncludePath(&command, "./usr/include/abc");
    ubSetSourceFile(&command, "./source/main.cpp");

    UBError err = ubBuild(&command);

    return err;
}
```

## Things to do
* Adding support for custom compilers
* Added nedded functions for it to be able to compile into .wasm
* Maybe a runig mode lookign for filechanges and auto compile mode.


## Thank you

This software was inspired of Tsodings nobuild system.
https://github.com/tsoding/nobuild


## Contributing
Dont. Clone it and build your ovn version of it instead.


## License
For open source projects, say how it is licensed.


## Project status
It is mostly experimental, its not done and work is done when needed and inspiration is found.
It may never be compleeted or it even may end up beeing a bad ide.
