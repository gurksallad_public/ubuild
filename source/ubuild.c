//  Example file for setting up and compiling 
//  your project specific buildtool

#define UBUILD_IMPLEMENTATION
#include "ubuild.h"



int main()
{
    cmd command = {};

    if(ubSetBuildOutput(&command, "Build", "MyAppName") == Error)
    {
        printf("Error creating folder....");
        return 1;
    }
    
    ubSetDefaultCompiler(&command);

    ubAddIncludePath(&command, "./usr/include/sdl");
    ubAddIncludePath(&command, "./usr/include/abc");

    ubSetSourceFile(&command, "./source/main.cpp");

    //ubPushCompilerFlags("-O2");
    //ubPushCompilerFlags("-WAll");

    //LinkerStuff..?
    
    UBError err = ubBuild(&command);

    return err;
}

void wasm()
{
    cmd command = {};

    //  clang --target=wasm32 -emit-llvm -c -S add.c
    //  llc -march=wasm32 -filetype=obj add.ll 
    //  wasm-ld --no-entry --export-all -o add.wasm add.o
}