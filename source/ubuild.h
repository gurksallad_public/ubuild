//  ubuild build too; Version 0.1
//  
//  API Documentation

//  Make sure to define the implementation befor including the header file
//  #define UBUILD_IMPLEMENTATION
//  #inlcude "ubuild.h"
//
//  ubSetBuildOutput("../Build/", "MyAppName");     // Sets to outputfolder and name of application
//  ubSetDefaultCompiler(&command);                 //  Sets default compiler on system, (GCC for Linux and CL for windows)
//  ubSetSourceFile("main.cpp");                    //  Set surcfile to compile
//  ubAddIncludePath("./usr/include/sdl");          //  Adds lookup path to includefiles (Any number is allowed)
//  ubPushCompilerFlags("-O2");                     //  Push additional compilerflags
//  ubBuild(cmd*)                                   //  Builds command and execute compilation and additional commands if added


#ifndef UBUILD_H
#define UBUILD_H

#include <stdio.h>
#include <stdlib.h>

#define MAXCOMMANDBUFFERSIZE 1024*1024

typedef enum UBError
{
    Success = 0,
    Error   = 1,
} UBError;

typedef enum UBCompilers
{
    Win_CL,
    GCC,
} UBCompiler;

typedef struct includePathNode
{
    char* Path;
    struct includePathNode *Next;
} includePathNode;

typedef struct cmd
{
    char *BuildPath;
    char *AppName;
    char *Compiler;
    char *SourceFile;
    includePathNode *IncludePaths;
} cmd;

int ubPushCommand(char*, char*);

/// HERE GOES IMPLEMENTATION
#ifndef UBUILD_IMPLEMENTED
#ifdef UBUILD_IMPLEMENTATION
#define UBUILD_IMPLEMENTED


/// @brief 
/// @param command 
/// @param path 
/// @param appName 
/// @return 
UBError ubSetBuildOutput(cmd *command, char *path, char *appName)
{
    command->AppName = appName;
    command->BuildPath = path;

    char buffer[1024];

    snprintf(buffer, 1024, "mkdir %s", path);
    int result = system(buffer);

    return (UBError)result;
}



/// @brief 
/// @param command 
/// @return 
UBError ubSetDefaultCompiler(cmd *command)
{
    #ifdef _WIN32 
        command->Compiler = "CL";
    #else
        command->Compiler = "GCC";
    #endif
}



/// @brief 
/// @param command 
/// @param file 
UBError ubSetSourceFile(cmd *command, char *file)
{
    command->SourceFile = file;

    return Success;
}



/// @brief Add folderlocation for includefiles
/// @param command 
/// @param path 
UBError ubAddIncludePath(cmd *command, char *path)
{
    includePathNode *newNode = (includePathNode*)malloc(sizeof(includePathNode));
    if(newNode == 0) return Error;

    newNode->Path = path;
    newNode->Next = 0;

    if(command->IncludePaths == 0)
    {
        command->IncludePaths = newNode;
        printf("asdasd");
    }
    else
    {
        includePathNode* node = command->IncludePaths;
        while(node->Next != 0)
        {
            node = node->Next;
        }
        node->Next = newNode;
        printf("22222");
    }
    return Success;
}



/// @brief 
/// @param command 
/// @return 
UBError ubBuild(cmd *command)
{
    char buffer[1024];  // temporary buffer
    char commandBuffer[MAXCOMMANDBUFFERSIZE];
    char *writer = &commandBuffer[0];

    // Set default compiler
    writer += ubPushCommand(writer, command->Compiler);
    
    // Add include paths
    includePathNode *node = command->IncludePaths;
    while(node != 0)
    {
        #ifdef _WIN32
        snprintf(buffer, 1024, "/I %s", node->Path);
        #else
        snprintf(buffer, 1024, "-I%s", node->Path);
        #endif
        writer += ubPushCommand(writer, buffer);
        node = node->Next;
    }

    // Add sourcefile to compile
    writer += ubPushCommand(writer, command->SourceFile);

    // Set application name
    #ifdef _WIN32
    snprintf(buffer, 1024, "/Fe%s", command->AppName);
    #else
    snprintf(buffer, 1024, "-o %s", command->AppName);
    #endif
    writer += ubPushCommand(writer, buffer);

    printf("Composit commad: %s\n", commandBuffer);

    printf("Buildtool Done!\n");
    return Success;
}



int ubPushCommand(char *w, char *command)
{
    printf("%s\n", command);
    return snprintf(w, MAXCOMMANDBUFFERSIZE, "%s ", command);
}

#endif
#endif
#endif